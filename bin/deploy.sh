#!/bin/bash

DOCKER_HOST="ssh://root@212.193.48.163" docker compose -f docker-compose.deploy.yml stop drupal
echo 'Delete files...'
DOCKER_HOST="ssh://root@212.193.48.163" docker compose -f docker-compose.deploy.yml exec server /bin/bash -O extglob -c 'cd /opt/drupal && rm -rf ./sync ./vendor ./web/!(sites|maintenance|index.php) ./web/sites/!(default) ./web/sites/default/!(files)'
echo 'Copy files...'
DOCKER_HOST="ssh://root@212.193.48.163" docker cp ./app/code/. loto-app-deploy_server_1:/opt/drupal/.
DOCKER_HOST="ssh://root@212.193.48.163" docker compose -f docker-compose.deploy.yml start drupal