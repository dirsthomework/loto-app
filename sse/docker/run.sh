#!/bin/bash

set -e

cd /app

[ ! -d "node_modules" ] && npm install

exec "$@"