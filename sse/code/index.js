'use strict'

const fs = require('fs');
const app = require('http').createServer(listener);
const sse = require('sse-broadcast')();
const {parse} = require('csv-parse/sync');

function notFound(req, res) {
    res.statusCode = 404
    res.end('Cannot ' + req.method + ' ' + req.url)
}

function listener(req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET');

    if (req.method === 'GET' && req.url === '/numbers/37') {
        sse.subscribe('channel-37', res);

        return;
    }

    if (req.method === 'GET' && req.url === '/numbers/38') {
        sse.subscribe('channel-38', res);

        return;
    }

    notFound(req, res);
}

function getLast10MinutesNumbers(loto = 37) {
    const csvContent = fs.readFileSync('/loto-files/web/sites/default/files/.private/loto' + loto + '.csv');

    let parsed = parse(csvContent, {
        columns: true,
        skip_empty_lines: true
    });

    parsed = parsed.slice(-1 * 50);

    let lastNumbers = [];

    const tenMinutesBack = new Date() - 60000 * 10;

    for (const oneLine of parsed) {
        // @ts-ignore
        if (oneLine.date >= tenMinutesBack) {
            lastNumbers.push({number: oneLine.number, time: oneLine.date});
        }
    }

    return lastNumbers;
}

setInterval(function () {
    sse.publish('channel-37', 'numbers', getLast10MinutesNumbers(37))
    sse.publish('channel-38', 'numbers', getLast10MinutesNumbers(38))
}, 1000);

app.listen(80)