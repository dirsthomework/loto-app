#!/bin/bash

mkdir -p ${APACHE_LOG_DIR}
fname=$(stat --format '%U' "$APACHE_LOG_DIR")
if [ "${fname}" != "$APACHE_USER" ]; then
  chown -R $APACHE_USER:$APACHE_USER $APACHE_LOG_DIR
fi

exec "httpd-foreground"