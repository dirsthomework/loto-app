<?php

namespace Drupal\loto_loader\Dto;

class NumberStat {

  public float $frequency= 0;

  public int $noShowUp = -1;

  public int $number;
}
