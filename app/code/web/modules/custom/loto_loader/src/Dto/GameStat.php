<?php

namespace Drupal\loto_loader\Dto;

class GameStat
{
  public float $averageClose;

  public float $maxClose;

  public int $winsCount;

  public int $betsCount;

  public float $betKoeff;
}
