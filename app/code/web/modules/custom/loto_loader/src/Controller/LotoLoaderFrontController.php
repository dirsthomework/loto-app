<?php

namespace Drupal\loto_loader\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;

class LotoLoaderFrontController extends ControllerBase {

  private const LAST_NUMBERS_COUNT = 40;

  public function render() {
    return [
      '#type' => 'markup',
      '#attached' => [
        'library' => ['loto_loader/style'],
      ],
      '#cache' => ['max-age' => Cache::PERMANENT, 'tags' => ['loto']],
      '#markup' => $this->buildLotoFrontBlocks(),
    ];
  }

  protected function buildLotoFrontBlocks(): string {
    $count = self::LAST_NUMBERS_COUNT;
    $loto37 = "<h2 class='loto-title'>
        <a class='link-head' target=__blank href='https://betboom.ru/game/tennis37'>Tennis 37 (Последние {$count})</a>
      </h2>" . $this->buildLotoBlock(
        'loto37.csv'
      );
    $loto38 = "<h2 class='loto-title'>
        <a class='link-head' target=__blank href='https://betboom.ru/game/tennis38'>Tennis 38 (Последние {$count})</a>
      </h2>" . $this->buildLotoBlock(
        'loto38.csv'
      );

    return $loto37 . $loto38;
  }

  protected function buildLotoBlock(string $fileName): string {
    $numbers = $this->getNumbers($fileName);

    if (empty($numbers)) {
      return "<div class='numbers-block'>Нет данных...</div>";
    }

    $lastNumbers = array_slice(
      $numbers,
      -1 * self::LAST_NUMBERS_COUNT
    );

    return $this->getNumbersBlock(array_reverse($lastNumbers));
  }

  protected function getNumbersBlock(array $numbers): string {
    $output = "<div class='numbers-block'>";
    foreach ($numbers as $number) {
      $output .= "<div class='number'>$number</div>";
    }
    $output .= "</div>";

    return $output;
  }


  protected function getNumbers(string $fileName): array {
    $numbersContent = file('private://' . $fileName);

    $csv = array_map(
      function ($oneLine) {
        $one = str_getcsv($oneLine);

        return $one[0];
      },
      $numbersContent ? $numbersContent : []
    );
    array_shift($csv);

    return $csv;
  }

}
