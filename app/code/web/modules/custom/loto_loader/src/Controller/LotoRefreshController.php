<?php

namespace Drupal\loto_loader\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class LotoRefreshController extends ControllerBase {

  private const LAST_NUMBERS_COUNT = 40;

  public function getNewNumbers(int $lotoVersion = 37, int $timestamp = 0) {
    $numbers = $this->getNumbers('loto' . $lotoVersion . '.csv');
    $responseArray = array_filter($numbers, function (array $one) use ($timestamp) {
      return $one['time'] > $timestamp;
    });

    return new JsonResponse(array_values($responseArray));
  }


  protected function getNumbers(string $fileName): array {
    $csv = array_map(
      function ($oneLine) {
        $one = str_getcsv($oneLine);

        return [
          'number' => $one[0],
          'time' => $one[1],
        ];
      },
      file('private://' . $fileName)
    );
    array_shift($csv);

    return $csv;
  }


}
