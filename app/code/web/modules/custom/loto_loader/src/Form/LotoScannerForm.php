<?php

namespace Drupal\loto_loader\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\loto_loader\Dto\NumberStat;
use Drupal\loto_loader\Gamer;
use Psr\Container\ContainerInterface;

class LotoScannerForm extends FormBase {

  protected Gamer $gamer;

  public function __construct(Gamer $gamer) {
    $this->gamer = $gamer;
  }

  public static function create(ContainerInterface $container) {
    return new static($container->get('loto_loader.gamer'));
  }

  public function getFormId() {
    return 'loto_scanner_form';
  }

  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    int $lotoVersion = NULL
  ) {

    $form['number-no-show'] = [
      '#type' => 'number',
      '#title' => 'Введите количество невыпадений',
      '#description' => 'Пример: если вы введете 50, то увидите все цифры которые давно не выпадали (более 50 раз подряд)',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Поиск',
    ];

    $form['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Сбросить'),
      '#submit' => ['::reset'],
    ];

    $statNumbers = $this->getStat(
      $lotoVersion,
      is_numeric(
        $form_state->getValue('number-no-show')
      ) ? $form_state->getValue('number-no-show') : 0
    );

    $form['stat'] = [
      '#theme' => 'loto_scanner_numbers_stat',
      '#numbers_stat' => $statNumbers,
      '#game_stat' => $this->gamer->getGameStat(
        array_keys($statNumbers),
        $lotoVersion
      ),
    ];

    $form['table'] = [
      '#theme' => 'loto_scanner',
      '#numbers' => $this->getFilteredNumbers(
        $lotoVersion,
        $form_state->getValue('start_date'),
        $form_state->getValue('end_date'),
      ),
      '#cache' => ['max-age' => 60],
    ];

    $form['#attached']['library'][] = 'loto_loader/style';
    $form['#attached']['library'][] = 'loto_loader/refresh';
    $form['#attached']['drupalSettings']['lotoVersion'] = $lotoVersion;

    //    $form['#after_build'] = ['::afterBuild'];
    //
    //    $form['#method'] = 'GET';

    return $form;
  }

  //  public function afterBuild(array $element, FormStateInterface $form_state) {
  //    unset($element['form_token']);
  //    unset($element['form_build_id']);
  //    unset($element['form_id']);
  //
  //    return $element;
  //  }

  protected function getStat(int $lotoVersion, int $noShowUps = 0): array {
    $allNumbers = array_reverse(
      $this->getNumbers('loto' . $lotoVersion . '.csv')
    );

    /* @var \Drupal\loto_loader\Dto\NumberStat[] $lotoNumbers */
    $lotoNumbers = array_pad([], $lotoVersion + 1, NULL);
    unset($lotoNumbers[0]);

    if (empty($allNumbers)) {
      return [];
    }

    foreach ($allNumbers as $position => $oneNumber) {
      if (!$lotoNumbers[$oneNumber['number']]) {
        $lotoNumbers[$oneNumber['number']] = new NumberStat();
        $lotoNumbers[$oneNumber['number']]->number = $oneNumber['number'];
        $lotoNumbers[$oneNumber['number']]->noShowUp = $position;
      }

      $lotoNumbers[$oneNumber['number']]->frequency++;
    }

    foreach ($lotoNumbers as $number => &$numberDto) {
      $numberDto->frequency /= count($allNumbers);
    }

    if ($noShowUps > 0) {
      $lotoNumbers = array_filter(
        $lotoNumbers,
        function (NumberStat $number) use ($noShowUps) {
          return $number->noShowUp >= $noShowUps;
        }
      );
    }

    return $lotoNumbers;
  }


  protected function getFilteredNumbers(
    int $lotoVersion,
    DrupalDateTime $dateStart = NULL,
    DrupalDateTime $dateEnd = NULL
  ): array {
    $allNumbers = array_reverse(
      $this->getNumbers('loto' . $lotoVersion . '.csv')
    );

    if (!$dateStart && !$dateEnd) {
      return array_slice($allNumbers, 0, 1000);
    }

    $dateStart = $dateStart ?? new DrupalDateTime('1.01.2000');
    $dateEnd = $dateEnd ?? new DrupalDateTime();

    $dateStart->setTime(0, 0, 0);
    $dateEnd->setTime(23, 59, 59);

    $filteredNumbers = [];
    foreach ($allNumbers as $oneNumber) {
      if ($oneNumber['time'] / 1000 >= $dateStart->getTimestamp(
        ) && $oneNumber['time'] / 1000 <= $dateEnd->getTimestamp()) {
        $filteredNumbers[] = $oneNumber;
      }
    }

    return $filteredNumbers;
  }

  protected function getNumbers(string $fileName): array {
    $csv = array_map(
      function ($oneLine) {
        $one = str_getcsv($oneLine);

        return [
          'number' => $one[0],
          'time' => $one[1],
        ];
      },
      file('private://' . $fileName)
    );
    array_shift($csv);

    return $csv;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $datesPassed = $form_state->getValue('start_date') && $form_state->getValue(
        'end_date'
      );

    if ($datesPassed && $form_state->getValue('start_date')->getTimestamp(
      ) > $form_state->getValue('end_date')->getTimestamp()) {
      $form_state->setError(
        $form['start_date'],
        'Дата начала не может быть больше даты окончания'
      );
    }

    if ($form_state->getValue('number-no-show') && $form_state->getValue(
        'number-no-show'
      ) < 0) {
      $form_state->setError(
        $form['number-no-show'],
        'Задайте значение больше 0'
      );
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

  public function reset(array &$form, FormStateInterface $form_state) {

  }

}
