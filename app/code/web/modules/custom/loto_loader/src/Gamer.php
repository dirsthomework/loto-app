<?php

namespace Drupal\loto_loader;

use Drupal\loto_loader\Dto\GameStat;

class Gamer {

  protected CONST BET_KOEFF = 36;

  public function getGameStat(
    array $numbersInPlay,
    int $lotoVersion = 37
  ): ?GameStat {
    $gameNumbers = $this->getNumbers('loto' . $lotoVersion . '.csv');

    $betsCounter = 0;
    $betsCountListBeforeWin = [];
    foreach ($gameNumbers as $number) {
      $betsCounter++;

      if (in_array($number, $numbersInPlay)) {
        $betsCountListBeforeWin[] = $betsCounter;
        $betsCounter = 0;
      }
    }

    if (empty($betsCountListBeforeWin)) {
      return NULL;
    }

    $stat = new GameStat();
    $stat->maxClose = max($betsCountListBeforeWin);
    $stat->averageClose = array_sum($betsCountListBeforeWin) / count(
        $betsCountListBeforeWin
      );
    $stat->winsCount = count($betsCountListBeforeWin);
    $stat->betsCount = array_sum($betsCountListBeforeWin);

    $stat->betKoeff = self::BET_KOEFF / count($numbersInPlay);

    return $stat;
  }

  protected function getNumbers(string $fileName): array {
    $numbersContent = file('private://' . $fileName);

    $csv = array_map(
      function ($oneLine) {
        $one = str_getcsv($oneLine);

        return $one[0];
      },
      $numbersContent ? $numbersContent : []
    );
    array_shift($csv);

    return $csv;
  }

}
