(function ($, settings) {
  $(document).ready(function () {
    const source = new EventSource( '/numbers/' + drupalSettings.lotoVersion, {withCredentials: false});
    source.addEventListener('numbers', function (event) {
      const numbers = JSON.parse(event.data);

      const lastPageNumberDate = $('.numbers-scanner-block.big .number').first().data('time');
      for (let number of numbers) {

        if (number.time <= lastPageNumberDate) {
          continue;
        }

        $('.numbers-scanner-block.big').prepend(
          $(
            '<div class="number new"  data-time="' + number.time + '">' + number.number + '<span class="date">' + (new Date(parseInt(number.time))).toLocaleString('ru', {timeZone: 'Europe/Moscow'}) + '</span></div>'
          ).hide().delay(500).show('slow')
        );
      }

    }, false);
  });

}(jQuery, drupalSettings));
