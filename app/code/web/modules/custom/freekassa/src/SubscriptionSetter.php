<?php

namespace Drupal\freekassa;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\role_expire\RoleExpireApiService;
use Drupal\user\UserInterface;

class SubscriptionSetter {

  private const SUBSCRIPTION_DAYS_LENGTH = 30;

  protected RoleExpireApiService $roleExpireApiService;

  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(
    RoleExpireApiService $roleExpireApiService,
    EntityTypeManager $entityTypeManager
  ) {
    $this->roleExpireApiService = $roleExpireApiService;
    $this->entityTypeManager = $entityTypeManager;
  }

  public function setNewSubscription(UserInterface $user): void {
    $endTimestamp = $this->calculateEndDateTimestamp($user->id());

    $this->createNewSubscription($user, $endTimestamp);

    $this->assignUserRoles($user, $endTimestamp);
  }

  protected function calculateEndDateTimestamp(int $userId): int {
    $storageQuery = $this->entityTypeManager->getStorage('node')
      ->getAggregateQuery();

    $subscriptions = $storageQuery->condition('type', 'podpiska')
      ->condition('status', 1)
      ->condition('field_data_okonchaniya', time(), '>')
      ->condition('field_polzovatel', $userId)
      ->aggregate('field_data_okonchaniya', 'max')
      ->execute();

    $endDate = time();
    if (current($subscriptions)['field_data_okonchaniya_max']) {
      $endDate = strtotime(
        current($subscriptions)['field_data_okonchaniya_max']
      );
    }

    return $endDate;
  }

  protected function createNewSubscription(
    UserInterface $user,
    int $endTimestamp
  ): void {
    $newSubscription = $this->entityTypeManager->getStorage('node')->create(
      [
        'title' => 'Подписка для ' . $user->getEmail(),
        'status' => 1,
        'type' => 'podpiska',
        'field_polzovatel' => $user->id(),
        'field_data_okonchaniya' => date('Y-m-d\TH:i:s', static::SUBSCRIPTION_DAYS_LENGTH * 24 * 60 * 60 + $endTimestamp),
      ]
    );
    $newSubscription->save();
  }

  protected function assignUserRoles(
    UserInterface $user,
    int $endTimestamp
  ): void {
    $user->addRole('loto_37_subscriber');
    $user->addRole('loto_38_subscriber');
    $user->save();

    $this->roleExpireApiService->writeRecord(
      $user->id(),
      'loto_37_subscriber',
      static::SUBSCRIPTION_DAYS_LENGTH * 24 * 60 * 60 + $endTimestamp
    );
    $this->roleExpireApiService->writeRecord(
      $user->id(),
      'loto_38_subscriber',
      static::SUBSCRIPTION_DAYS_LENGTH * 24 * 60 * 60 + $endTimestamp
    );
  }

}
