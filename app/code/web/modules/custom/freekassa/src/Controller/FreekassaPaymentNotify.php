<?php

namespace Drupal\freekassa\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\freekassa\Form\PaymentForm;
use Drupal\freekassa\SubscriptionSetter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;


class FreekassaPaymentNotify extends ControllerBase {

  private const VALID_IP = [
    '168.119.157.136',
    '168.119.60.227',
    '138.201.88.124',
    '178.154.197.79',
  ];

  private const SECRET2 = 'ZW?0sHje2Gxy,BO';

  protected LoggerChannelInterface $logger;

  protected SubscriptionSetter $subscriptionSetter;

  public function __construct(
    LoggerChannelInterface $logger,
    SubscriptionSetter $subscriptionSetter
  ) {
    $this->logger = $logger;
    $this->subscriptionSetter = $subscriptionSetter;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.channel.freekassa'),
      $container->get('freekassa.subscription_setter')
    );
  }

  public function accept(Request $request): void {
    $this->validateFreekassaRequest($request);

    /* @var \Drupal\user\UserInterface $user */
    $user = $this->getUserFromRequest($request);

    $this->subscriptionSetter->setNewSubscription($user);

    die('YES');
  }

  public function success() {
    return [
      '#markup' => 'Оплата прошла успешно, доступ откроется через несколько минут, если этого не произошло напишите нам.'
    ];
  }

  public function failure() {
    return [
      '#markup' => 'Что-то пошло не так, если оплатить не выходит - напишите нам.'
    ];
  }

  public function validateFreekassaRequest(Request $request) {
    $requestContent = $request->request->all();

    if (empty($requestContent)) {
      $this->logger->warning(
        'Interaction URL accessed with no POST data submitted.'
      );

      throw new \Exception();
    }

    $this->validateIp($request);
    $this->validateFreekassaRequestFields($requestContent);
  }

  protected function validateIp(Request $request): void {
    $ip = $request->server->get('HTTP_CF_CONNECTING_IP');

    if (!in_array($ip, static::VALID_IP)) {
      $this->logger->warning('Invalid IP address');

      throw new \Exception('Invalid IP address');
    }
  }

  protected function validateFreekassaRequestFields(array $requestContent
  ): void {
    // Exit now if any required keys are not exists in $_POST.
    $required_keys = ['AMOUNT', 'MERCHANT_ORDER_ID', 'SIGN'];

    $unavailable_required_keys = array_diff_key(
      array_flip($required_keys),
      $requestContent
    );
    if (!empty($unavailable_required_keys)) {
      $this->logger->warning(
        'Missing POST keys. POST data: <pre>!data</pre>',
        ['!data' => print_r($unavailable_required_keys, TRUE)]
      );

      throw new \Exception();
    }


    $request_sign = $requestContent['SIGN'];
    $amount = number_format(PaymentForm::AMOUNT, 2, '.', '') * 1;
    $sign = md5(
      implode(":", [
        PaymentForm::MERCHANT,
        $amount,
        static::SECRET2,
        $requestContent['MERCHANT_ORDER_ID'],
      ])
    );

    if ((string) $request_sign !== $sign) {
      $this->logger->warning(
        'Missing Signature. 1 POST data: !data',
        ['!data' => print_r($requestContent, TRUE)]
      );

      throw new \Exception(print_r($sign, TRUE));
    }
  }

  protected function getUserFromRequest(Request $request): EntityInterface {
    $requestContent = $request->request->all();

    // LOAD USER FROM MERCHANT_ORDER_ID
    $user = $this->entityTypeManager()->getStorage('user')->loadByProperties(
      ['mail' => $requestContent['MERCHANT_ORDER_ID']]
    );

    if (empty($user)) {
      $this->logger->warning(
        'No user with email = ' . $requestContent['MERCHANT_ORDER_ID']
      );

      throw new \Exception('No such user');
    }

    return current($user);
  }

}
