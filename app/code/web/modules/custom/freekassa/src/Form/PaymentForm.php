<?php

namespace Drupal\freekassa\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;

class PaymentForm extends FormBase {

  public const MERCHANT = 16062;

  public const AMOUNT = 300;

  private const CURRENCY = 'RUB';

  private const SECRET1 = 'pujp!NgKi/xjZ/j';

  private const ENDPOINT = 'https://pay.freekassa.ru/';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'payment_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'markup',
      '#markup' => "Подписка оформляется на <strong>30</strong> дней. Стоимость подписки составляет <strong>".self::AMOUNT."</strong> рублей.
        <br><i>Вы получаете полный доступ к статистикам Tennis 37 и Tennis 38</i><br>",
    ];

    $form['buy'] = [
      '#type' => 'submit',
      '#value' => 'Купить',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $data["Merchant_id"] = static::MERCHANT;
    $data["AMOUNT"] = number_format(static::AMOUNT, 2, '.', '') * 1;
    $data['currency'] = static::CURRENCY;
    $data["MERCHANT_ORDER_ID"] = \Drupal::currentUser()->getEmail();

    // Calculate signature.
    $sign = md5(
      implode(":", [
        $data['Merchant_id'],
        $data['AMOUNT'],
        static::SECRET1,
        $data['currency'],
        $data['MERCHANT_ORDER_ID'],
      ])
    );

    $redirectUrl = static::ENDPOINT . "?m={$data["Merchant_id"]}&oa={$data["AMOUNT"]}&currency={$data['currency']}&o={$data["MERCHANT_ORDER_ID"]}&s={$sign}";

    $response = new TrustedRedirectResponse($redirectUrl);
    $form_state->setResponse($response);
  }

}
