#!/bin/bash

set -e

[ ! -d "/opt/drupal/web/sites/default/files" ] && mkdir /opt/drupal/web/sites/default/files
[ ! -d "/opt/drupal/web/sites/default/files/.private" ] && mkdir /opt/drupal/web/sites/default/files/.private

chown -R user:user /opt/drupal/web/sites/default/files/.private

composer install

drush cr
drush state:set system.maintenance_mode 1 --input-format=integer
drush updb -y
drush cim -y
drush sql:query 'truncate semaphore'
drush state:set system.maintenance_mode 0 --input-format=integer
drush cr

exec php-fpm